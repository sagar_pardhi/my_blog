---
layout: post
title: "What is Golang and How to install it?"
date: 2019-11-14
---
![golang](https://hackernoon.com/hn-images/0*fyyS1OHEaQ2il8Tg.png)

## Introduction


`Golang`, also know as `Go`, is a statically typed, compiled programming language designed at Google in 2007 by Robert Griesemer, Rob Pike, and Ken Thompson. Go is syntactically similar to C, but with memory safety, garbage collection, structural typing, and CSP-style concurrency. It was first appeared in November 10, 2009 to public. `Go` is was written in C, but now written in `Go` itself. While the latter is a gcc frontend written mainly in `C++`. It is an open source programming language that makes it easy to build simple, reliable, and efficient software. Go includes many features of other modern languages, such as `method` and `operator overloading`, `pointer` `arithmetic`, and `type inheritance`.

## Installing Golang

[Official binary distributions](https://golang.org/dl/) are available for the FreeBSD (release 10-STABLE and above), Linux, macOS (10.10 and above), and Windows operating systems and the 32-bit (386) and 64-bit (amd64) x86 processor architectures. Before installing `Golang` check out the System Requirements as follows:

### System Requirements

Operating system | Architectures | Notes
FreeBSD 10.3 or later | amd64, 386 | Debian GNU/kFreeBSD not supported
Linux 2.6.23 or later with glibc | amd64, 386, arm, arm64, s390x, ppc64le | CentOS/RHEL 5.x not supported.Install from source for other libc.
macOS 10.10 or later | amd64 | use the clang or gcc† that comes with Xcode‡ for cgo support
Windows 7, Server 2008R2 or later | amd64, 386 | use MinGW (386) or MinGW-W64 (amd64) gcc†. No need for cygwin or msys.

* A C compiler is required only if you plan to use cgo.
* On Mac you need to install the command line tools for Xcode. If you have already installed Xcode 4.3+, you can install it from the Components tab of the Downloads preferences panel.

## Install the Go tools

If you are upgrading from an older version of Go you must first remove the existing version.

#### Linux, macOS, and FreeBSD tarballs

[Download the archive](https://golang.org/dl/) and extract it into `/usr/local`, creating a Go tree in `/usr/local/go`. For example:
```
tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz
```

Choose the archive file appropriate for your installation. For instance, if you are installing Go version 1.2.1 for 64-bit x86 on Linux, the archive you want is called go1.2.1.linux-amd64.tar.gz.

(Typically these commands must be run as root or through sudo.)

Add `/usr/local/go/bin` to the PATH environment variable. You can do this by adding this line to your `/etc/profile` (for a system-wide installation) or `$HOME/.profile`:

```
export PATH=$PATH:/usr/local/go/bin
```

Then just run the shell commands directly or execute them from the profile using a command such as source `$HOME/.profile`.

#### macOS package installer

[Download the package](https://golang.org/dl/) file, open it, and follow the prompts to install the Go tools. The package installs the Go distribution to `/usr/local/go`.

The package should put the `/usr/local/go/bin` directory in your PATH environment variable. You may need to restart any open Terminal sessions for the change to take effect.

#### Windows

The Go project provides two installation options for Windows users (besides [installing from source](https://golang.org/doc/install/source)): a zip archive that requires you to set some environment variables and an MSI installer that configures your installation automatically.

##### MSI installer

Open the [MSI file](https://golang.org/dl/) and follow the prompts to install the Go tools. By default, the installer puts the Go distribution in c:\Go.

The installer should put the `c:\Go\bin` directory in your PATH environment variable. You may need to restart any open command prompts for the change to take effect.

##### Zip archive

Download the zip file and extract it into the directory of your choice (i suggest `c:\Go`).

Add the bin subdirectory of your Go root (for example, `c:\Go\bin`) to your PATH environment variable.

##### Setting environment variables under Windows

> Under Windows, you may set environment variables through the "Environment Variables" button on the "Advanced" tab of the "System" control panel. Some versions of Windows provide this control panel through the "Advanced System Settings" option inside the "System" control panel.

## Test your installation

Check that Go is installed correctly by setting up a workspace and building a simple program, as follows.

Create your workspace directory, `%USERPROFILE%\go`. (If you'd like to use a different directory, you will need to set the GOPATH environment variable.)

Next, make the directory `src\hello` inside your workspace, and in that directory create a file named `hello.go` that looks like:

```golang
package main

import "fmt"

func main() {
    fmt.Printf("hello, world\n")
}
```

Then build it with the go tool:

```
C:\> cd %USERPROFILE%\go\src\hello
C:\Users\Gopher\go\src\hello> go build
```

The command above will build an executable named hello.exe in the directory alongside your source code. Execute it to see the greeting:

```
C:\Users\Gopher\go\src\hello> hello
hello, world
```

If you see the "hello, world" message then your Go installation is working.

You can run `go install` to install the binary into your workspace's bin directory or `go clean -i` to remove it.

Before rushing off to write Go code please read the [How to Write Go Code](https://golang.org/doc/code.html) document, which describes some essential concepts about using the Go tools.

## Uninstalling Go

To remove an existing Go installation from your system delete the go directory. This is usually `/usr/local/go` under Linux, macOS, and FreeBSD or `c:\Go` under Windows.

You should also remove the Go bin directory from your PATH environment variable. Under Linux and FreeBSD you should edit `/etc/profile` or `$HOME/.profile`. If you installed Go with the [macOS package](https://golang.org/doc/install#macos) then you should remove the `/etc/paths.d/go` file. Windows users should read the section about [setting environment variables under Windows.](https://golang.org/doc/install#windows_env)

You can further continue reading the official [Docs](https://golang.org/) if you want to get deeper knowledge of `Go`.